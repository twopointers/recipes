# Contributors Guide

**Want to contribute? Fantastic!** \
I am currently working out the documentation and process to report bugs, recipe suggestions and general all around processes for this to happen.  Any suggestions are welcomed. Thanks for your interest.

## Legal

All original contributions or recipes will be licensed under the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
