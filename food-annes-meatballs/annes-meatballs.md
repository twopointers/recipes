<p style="color:gray">food-annes-meatballs</p>

# Anne’s Amazing Meatballs
Makes approximately 16 meatballs

## Ingredients
&emsp; 1 lb ground beef \
&emsp; 1 lb ground pork \
&emsp; 2 eggs \
&emsp; 3 cloves garlic, minced \
&emsp; 1 cup plain bread crumbs \
&emsp; 2 tablespoons parsley, chopped \
&emsp; 1/4 cup romano cheese, grated \
&emsp; 1/2 teaspoon salt \
&emsp; 1/2 teaspoon pepper

---

[notes](#notes "read recipe notes")&emsp;|&emsp;[images](#images "view images of recipe")&emsp;|&emsp;[preferred brands](#preferred-brands "my preferred brands")&emsp;|&emsp;Orginal Recipe/Video: [https://www.youtube.com/watch?v=Q0xVDLe_Jfw](https://www.youtube.com/watch?v=Q0xVDLe_Jfw)

### Directions
Preheat the oven to 375&deg;F.

Mix all ingredients in a large bowl, then form into meatballs slightly larger than a golf ball.

Place meatballs on a wire rack on a parchment lined baking sheet and bake for 20 minutes on a middle rack.

If using right away put the meatballs into a marinara sauce and cook for an additional 30 minutes, or freeze them for another time.

#### Notes
* Goes great with my [americanara sauce](https://www.twopointers.com/food/americanara-sauce/) (marinara sauce).
* To freeze for later, place meatballs into a freezer bag after the inital baking time and the meatballs have cooled.  When ready, defrost the meatballs and finish by cooking the meatballs in sauce for 30 minutes.

##### Images
![Annes Meatballs](media/annes-meatballs-gitlab.jpg "Image of Annes Meatballs")

###### Preferred Brands
romano cheese - [Pecorino Romano](https://en.wikipedia.org/wiki/Pecorino_Romano) \
bread crumbs - [Progresso](https://www.progresso.com/products/bread-crumbs-plain/)&reg; Plain Bread Crumbs

>**I DO NOT** get any compensation, free stuff or acknowledgment for mentioning, using or linking to any specific products mentioned. Any product or ingredient in the recipe can be substituted with what you have available, a regional favorite or what you find delicious.

---

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/ "Creative Commons License") \
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/ "license").